<?php 
add_action('plugins_loaded', 'HokBay_Dropshipping_Oders_init');

function HokBay_Dropshipping_Oders_init() {
	global $ppp;
	$ppp->orders = new HokBay_Dropshipping_Oders();
}

class HokBay_Dropshipping_Oders extends WC_Dropshipping_Orders {
	public function __construct() {
		$this -> unregister_parent_hook();
		add_action('woocommerce_order_status_processing',array($this,'hokbay_order_processing'));
	}

	public function unregister_parent_hook() {
		remove_action('woocommerce_order_status_processing',array($this,'order_processing'));
	}

	// perform all tasks that happen once an order is set to processing
	public function hokbay_order_processing( $order_id ) {
		$order = new WC_Order( $order_id ); // load the order from woocommerce
		$this->hokbay_notify_warehouse($order); // notify the warehouse to ship the order
	}

	// parse the order, build pdfs, and send orders to the correct suppliers
	public function hokbay_notify_warehouse( $order ) {
		$order_info = $this->hokbay_get_order_info($order);
		$supplier_codes = $order_info['suppliers'];
		// for each supplier code, loop and send email with product info
		foreach($supplier_codes as $code => $supplier_info) {
			do_action('wc_dropship_manager_send_order',$order_info,$supplier_info);
		}
	}

	public function get_order_info($order) {
		// gather some of the basic order info
		$order_info = array();
		$order_info['id'] = $order->get_order_number();
		$order_info['number'] = $order->get_order_number();
		$order_info['options'] = get_option( 'wc_dropship_manager' );
		$order_info['shipping_info'] = $this->get_order_shipping_info($order);
		$order_info['billing_info'] = $this->get_order_billing_info($order);
		$order_info['order'] = $order;
		// for each item determine what products go to what suppliers.
		// Build product/supplier lists so we can send send out our order emails
		$order_info['suppliers'] = array();
		$items = $order->get_items();
		if ( count( $items ) > 0 ) {
			foreach( $items as $item_id => $item ) {
				$ds = hokbay_dropshipping_get_dropship_supplier_by_product_id( intval( $item['product_id'] ) );
				if ($ds['id'] > 0) {
					$product = $order->get_product_from_item( $item ); // get the product obj
					$prod_info = $this->get_order_product_info($item,$product);
					if(!array_key_exists($ds['slug'],$order_info['suppliers']))
					{
						$order_info['suppliers'][$ds['slug']] = $ds;  // ...add newly found dropship_supplier to the supplier array
						$order_info[$ds['slug']] = array(); // ... and create an empty array to store product info in
					}
					$order_info[$ds['slug']][] = $prod_info;
					//$order_info[$ds['slug'].'_raw'][] = $product;
				}
			}
		} else {
			// how did we get here?
			//$this->sendAlert('No Products found for order #'.$order_info['id'].'!');
			//die;
		}
		return $order_info;
	}

	function hokbay_dropshipping_get_dropship_supplier ( $id = '' ) {
		$term = get_term( intval( $id ),'dropship_supplier' );

		if ( !isset( $term->term_id ) ) {
			$term = get_term_by('slug', 'roc','dropship_supplier');
			//error_log('2333');
			//error_log($term->name);
		}
		$supplier = get_woocommerce_term_meta( intval( $term->term_id ), 'meta', true );
		$supplier['id'] = $term->term_id;
		$supplier['slug'] = $term->slug;
		$supplier['name'] = $term->name;
		$supplier['description'] = $term->description;
		return $supplier;
	}

	function hokbay_dropshipping_get_dropship_supplier_by_product_id ( $product_id ) {
		$supplier = array();
		$terms = get_the_terms( intval( $product_id ), 'dropship_supplier' );
		if ( 0 < count( $terms ) ) {
			$supplier = wc_dropshipping_get_dropship_supplier( intval( $terms[0]->term_id ) ); // load the term. there can only be one supplier notified per product
		}
		return $supplier;
	}


}