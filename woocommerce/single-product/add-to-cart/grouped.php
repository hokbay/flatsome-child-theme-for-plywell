<?php
/**
 * Grouped product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/grouped.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.7
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $post;

$parent_product_post = $post;

do_action( 'woocommerce_before_add_to_cart_form' ); ?>



<style>
.grouptable
{
	display:table;
	border-collapse:separate;
	
}
.groupthead
{
	display:table-header-group;
	color:white;
	font-weight:100%;
	background-color:#29B543;
	line-height: 30px;
    vertical-align: middle;

}
.grouptbody
{
	display:table-row-group;
	
}
.grouptr
{
	display:table-row;

}
.grouptd
{
	display:table-cell;
	
	padding:6px;
	border-bottom: 1px dotted #e5e5e5 !important;
}
</style>


<div class="grouptable">
	<div class="groupthead">
		<div class="grouptr">
			<div class="grouptd">SKU#</div>
			<div class="grouptd">Description</div>
			<div class="grouptd">Price</div>
			<div class="grouptd">Quantity</div>
			<div class="grouptd"></div>
			
			
		</div>
	</div>
    
	<div class="grouptbody">
		
					<?php
						$quantites_required = false;
						$previous_post      = $post;

						foreach ( $grouped_products as $grouped_product ) {
							$post_object        = get_post( $grouped_product->get_id() );
							$quantites_required = $quantites_required || ( $grouped_product->is_purchasable() && ! $grouped_product->has_options() );

							setup_postdata( $post = $post_object );
							?>
							<form class="cart grouptr" method="post" enctype='multipart/form-data'>
									
									<div class="grouptd label" style="word-break: break-all;width:450px"> 
										<label for="product-<?php echo $grouped_product->get_id(); ?>">
											<?php echo $grouped_product->is_visible() ? '<a href="' . esc_url( apply_filters( 'woocommerce_grouped_product_list_link', get_permalink( $grouped_product->get_id() ), $grouped_product->get_id() ) ) . '">' . $grouped_product->get_sku() . '</a>' : $grouped_product->get_sku(); ?>
										</label>
									</div>
									<div class="grouptd label" style="word-break: break-all;width:500px">
													<span for="product-<?php echo $product_id; ?>">
														<?php echo $grouped_product->is_visible() ? '<a href="' . esc_url( apply_filters( 'woocommerce_grouped_product_list_link', get_permalink(), $grouped_product->$product_id ) ) . '">' . $grouped_product->post->post_excerpt . '</a>' : $grouped_product->post->post_excerpt; ?>
													</span>
									</div>
									<?php do_action( 'woocommerce_grouped_product_list_before_price', $grouped_product ); ?>
									<div class="grouptd" style="word-break: break-word;width:250px">
										<?php
											echo $grouped_product->get_price_html();
											echo wc_get_stock_html( $grouped_product );
										?>
									</div>
									<div class="grouptd" style="word-break: break-all;width:130px">
										<?php if ( ! $grouped_product->is_purchasable() || $grouped_product->has_options() ) : ?>
											<?php woocommerce_template_loop_add_to_cart(); ?>

										<?php elseif ( $grouped_product->is_sold_individually() ) : ?>
											<input type="checkbox" name="<?php echo esc_attr( 'quantity[' . $grouped_product->get_id() . ']' ); ?>" value="1" class="wc-grouped-product-add-to-cart-checkbox" />

										<?php else : ?>
											<?php
												/**
												 * @since 3.0.0.
												 */
												do_action( 'woocommerce_before_add_to_cart_quantity' );
												if(empty( $availability['availability'] )){
													woocommerce_quantity_input( array(
														'input_name'  => 'quantity[' . $grouped_product->get_id() . ']',
														'input_value' => isset( $_POST['quantity'][ $grouped_product->get_id() ] ) ? wc_stock_amount( $_POST['quantity'][ $grouped_product->get_id() ] ) : 0,
														'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $grouped_product ),
														'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $grouped_product->get_max_purchase_quantity(), $grouped_product ),
													) );
												}
												else{
													echo "<p style='color:red' class='stock'". esc_attr( $availability['class'] ). ">Out of stock"."</p>";
												}

												/**
												 * @since 3.0.0.
												 */
												do_action( 'woocommerce_after_add_to_cart_quantity' );
											?>
										<?php endif; 
											// Return data to original post.
											$post    = $parent_product_post;
											$product = wc_get_product( $parent_product_post->ID );
											setup_postdata( $post = $previous_post );
								        ?>
									</div>
									
					
									<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" />

									<?php if ( $quantites_required ) : ?>

										<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

										<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( "Add"); ?></button>

										<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

									<?php endif; ?>
								
							</form>
							<?php
								
						}
					?>

	</div>

</div>


<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
