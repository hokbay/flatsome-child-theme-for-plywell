<?php

//require_once get_stylesheet_directory() . '/inc/dropshipping.php';

add_action( 'after_setup_theme', 'replace_parent_actions' );
function replace_parent_actions(){

 //for changing the tabs' name in my account page
function wpb_woo_my_account_order() {
 $myorder = array(
 'dashboard' => __( 'Dashboard', 'woocommerce' ),
 'orders' => __( 'Orders', 'woocommerce' ),
 'edit-address' => __( 'Addresses', 'woocommerce' ),
 'edit-account' => __( 'Account Details', 'woocommerce' ),
 'customer-logout' => __( 'Logout', 'woocommerce' ),
 'wishlist-manage' => __( 'Wishlist', 'woocommerce' ),
 );
 return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );


/* Restricting shipping only to continental U.S. by removing certain states from default U.S. list */
add_filter( 'woocommerce_states', 'custom_woocommerce_states' );

function custom_woocommerce_states( $states ) {
$excluded_states = array('AK', 'AA', 'AE', 'AP', 'AS', 'GU', 'MP', 'PR', 'UM', 'VI', 'HI');
foreach($excluded_states as $no_state){
unset($states['US'][$no_state]);
}

return $states;
}

// Change comment subscription opt-in checkbox wording
//	remove_filter('comment_form_default_fields', 'cren_comment_fields');
//	remove_filter('comment_form_submit_field', 'cren_comment_fields_logged_in');
//	add_filter('comment_form_default_fields', 'hokbay_comment_fields');
//	add_filter('comment_form_submit_field', 'hokbay_comment_fields_logged_in');
}


// Change comment subscription opt-in checkbox wording
/*
function hokbay_comment_fields($fields) {
    $fields['cren_subscribe_to_comment'] = '<p class="comment-form-comment-subscribe">'.
      '<label for="cren_subscribe_to_comment"><input id="cren_subscribe_to_comment" name="cren_subscribe_to_comment" type="checkbox" value="on" checked>' . __('Receive e-mail notification for replys' , 'cren-plugin') . '</label></p>';

    return $fields;
}

function hokbay_comment_fields_logged_in($submitField) {
    if (is_user_logged_in()) {
        $checkbox = '<p class="comment-form-comment-subscribe">'.
            '<label for="cren_subscribe_to_comment"><input id="cren_subscribe_to_comment" name="cren_subscribe_to_comment" type="checkbox" value="on" checked>' . __('Receivce e-mail notification for replys', 'cren-plugin') . '</label></p>';
    }

    return $checkbox . $submitField;
}*/
